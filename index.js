let http = require('http');

let port= 3000;

http.createServer(function(request,response){

	response.write(' Welcome to port 3000');
	response.end();
}).listen(port);

console.log(`Server is running successfully on port: ${port}`);